# php-apache-oci

## Getting started
....

## Installation
Pull this image to your local machine.
~~~bash
docker pull jokomanza/php-apache-oci8:8.2
~~~

Then start the container.
~~~bash
docker run --restart=always -d -p 82:80 -v /Users/jokosupriyanto/php82:/app jokomanza/php-apache-oci8:8.2
~~~
